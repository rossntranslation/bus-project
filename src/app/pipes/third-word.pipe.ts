import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'thirdWord'
})
export class ThirdWordPipe implements PipeTransform {

  transform(value: string, args?: any): string {
    if (!value) {
      return "";
    }
    return value.split(" ")[2];
  }

}
 