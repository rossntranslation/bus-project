import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
  name: "secondWord"
})
export class SecondWordPipe implements PipeTransform {
  transform(value: string, args?: any): string {
    if (!value) {
      return "";
    }
    return value.split(" ")[1];
  }
}
