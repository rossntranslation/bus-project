import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'status'
})
export class StatusPipe implements PipeTransform {


  transform(value: number): string {
    if (value === null){
      return "unknown";
    }
    if (value <= 0 && !null){
      return "early";
    }
    else if (value >= 0 && value <= 300 && !null){
      return "on time";
    }  
    else if (value >= 301 && !null) {
      return "late";
    }     
    else {
      return "unknown";
    }
  }

}
