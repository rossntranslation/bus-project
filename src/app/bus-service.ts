export interface BusData {
    busId: string;
    routeVariant: string;
    deviationFromTimetable?: number;
}

export interface IBusService {
    organisation: string;
    date: string;
    busData: BusData[];
}

