import {throwError as observableThrowError,  Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { IBusService } from '../bus-service';
import { tap, catchError } from 'rxjs/operators';
import * as _ from 'lodash';

@Injectable()
export class BusService {

  private _url: string = "/assets/data/bus-service.json";

  constructor(private http:HttpClient) { }

  getBusService(): Observable<IBusService[]>{
    return this.http.get<IBusService[]>(this._url)
                    .pipe(tap(data => alert(JSON.stringify(data))) , catchError(this.errorHandler))
  }
  errorHandler(error: HttpErrorResponse){
    return observableThrowError(error.message || "Server Error");
  }


}