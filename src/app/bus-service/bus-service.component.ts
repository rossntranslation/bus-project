import { Component, OnInit } from "@angular/core";
import { BusService } from "../service/bus-service.service";
import { MatExpansionModule } from "@angular/material/expansion";

@Component({
  selector: "app-bus-service",
  templateUrl: "./bus-service.component.html",
  styleUrls: ["./bus-service.component.scss"]
})
export class BusServiceComponent implements OnInit {
  public buses = [];
  public errorMsg;
  busStatus: number;
  constructor(private _busService: BusService) {}

  ngOnInit() {
    this._busService
      .getBusService()
      .subscribe(data => (this.buses = data), error => (this.errorMsg = error));
  }

  getBusServiceClass(subItem): any {
    if (subItem.deviationFromTimetable === null) {
      return "status_unknown";
      console.log('subItem.routeVariant')
    }
    if (subItem.deviationFromTimetable <= 0) {
      return "status_early";
    } else if (
      subItem.deviationFromTimetable >= 0 &&
      subItem.deviationFromTimetable <= 300 &&
      !null
    ) {
      return "status_on-time";
    } else if (subItem.deviationFromTimetable >= 301) {
      return "status_late";
    }
  }
}
