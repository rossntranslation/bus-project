import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { BusServiceComponent } from './bus-service/bus-service.component';
import { BusService } from './service/bus-service.service';
import { MatExpansionModule } from '@angular/material/expansion';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FirstWordPipe } from './pipes/first-word.pipe';
import { SecondWordPipe } from './pipes/second-word.pipe';
import { StatusPipe } from './pipes/status.pipe';
import { ThirdWordPipe } from './pipes/third-word.pipe'
@NgModule({
  declarations: [
    AppComponent,
    BusServiceComponent,
    FirstWordPipe,
    SecondWordPipe,
    StatusPipe,
    ThirdWordPipe
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatExpansionModule
  ],
  providers: [BusService],
  bootstrap: [AppComponent]
})
export class AppModule { }
